library(tidyverse)



df <- read_csv("30_Auto_theft.csv", show_col_types = FALSE)

mydata1 <- na.omit(df)

new <- mydata1[!grepl("NULL", mydata1$Auto_Theft_Recovered),]

new$Auto_Theft_Recovered <- as.double(new$Auto_Theft_Recovered)

ratio <- aggregate(cbind(Auto_Theft_Recovered, Auto_Theft_Stolen) ~ Year + Group_Name, data = new, show_col_types = FALSE, FUN = sum, na.rm = TRUE)

DF2 = ratio[-c(51,52,53,54,55,56,57,58,59,60),]

x <- DF2$Auto_Theft_Stolen
y <- DF2$Auto_Theft_Recovered

cor.test(x, y,
         alternative = c("two.sided", "less", "greater"),
         method = c("pearson", "kendall", "spearman"))